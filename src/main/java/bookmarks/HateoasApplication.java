package bookmarks;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


@SpringBootApplication
public class HateoasApplication {

    public static void main(String[] args) {
        SpringApplication.run(HateoasApplication.class, args);
    }

    @Bean
    CommandLineRunner init(AccountRepository accountRepository,
                           BookmarkRepository bookmarkRepository) {
        return args ->
                Arrays.asList("ellen", "marjolein","florien","jhoeller","dsyer","pwebb","ogierke","rwinch","mfisher","mpollack","jlong")
                        .forEach(username -> {
                            Account account = accountRepository.save(new Account(username, "password"));
                            bookmarkRepository.save(new Bookmark(account, "http://bookmark.com/1/" + username, "A description"));
                            bookmarkRepository.save(new Bookmark(account, "http://bookmark.com/2/" + username, "A description"));
                        });
    }
}


@RestController
@RequestMapping("/bookmarks")
class BookmarkRestController {

    private final BookmarkRepository bookmarkRepository;
    private final AccountRepository accountRepository;

    BookmarkRestController(BookmarkRepository bookmarkRepository,
                           AccountRepository accountRepository) {
        this.bookmarkRepository = bookmarkRepository;
        this.accountRepository = accountRepository;
    }

    /**
     * Serve up a collection of links at the root URI for the client to consume.
     * @return
     */
    @GetMapping(produces = MediaTypes.HAL_JSON_VALUE)
    ResourceSupport root() {
        ResourceSupport root = new ResourceSupport();

        root.add(this.accountRepository.findAll().stream()
                .map(account -> linkTo(methodOn(BookmarkRestController.class)
                        .readBookmarks(account.getUsername()))
                        .withRel(account.getUsername()))
                .collect(Collectors.toList()));

        return root;
    }

    /**
     * Look up a collection of {@link Bookmark}s and transform then into a set of {@link Resources}.
     *
     * @param userId
     * @return
     */
    @GetMapping(value = "/{userId}", produces = MediaTypes.HAL_JSON_VALUE)
    Resources<Resource<Bookmark>> readBookmarks(@PathVariable String userId) {

        this.validateUser(userId);

        return new Resources<>(this.bookmarkRepository
                .findByAccountUsername(userId).stream()
                .map(bookmark -> toResource(bookmark, userId))
                .collect(Collectors.toList()));
    }

    @PostMapping("/{userId}")
    ResponseEntity<?> add(@PathVariable String userId, @RequestBody Bookmark input) {

        this.validateUser(userId);

        return this.accountRepository.findByUsername(userId)
                .map(account -> ResponseEntity.created(
                        URI.create(
                                toResource(
                                        this.bookmarkRepository.save(Bookmark.from(account, input)), userId)
                                        .getLink(Link.REL_SELF).getHref()))
                        .build())
                .orElse(ResponseEntity.noContent().build());
    }

    /**
     * Find a single bookmark and transform it into a {@link Resource} of {@link Bookmark}s.
     *
     * @param userId
     * @param bookmarkId
     * @return
     */
    @GetMapping(value = "/{userId}/{bookmarkId}", produces = MediaTypes.HAL_JSON_VALUE)
    Resource<Bookmark> readBookmark(@PathVariable String userId,
                                    @PathVariable Long bookmarkId) {
        this.validateUser(userId);

        return this.bookmarkRepository.findById(bookmarkId)
                .map(bookmark -> toResource(bookmark, userId))
                .orElseThrow(() -> new BookmarkNotFoundException(bookmarkId));
    }

    /**
     * Verify the {@literal userId} exists.
     *
     * @param userId
     */
    private void validateUser(String userId) {
        this.accountRepository
                .findByUsername(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
    }

    /**
     * Transform a {@link Bookmark} into a {@link Resource}.
     *
     * @param bookmark
     * @param userId
     * @return
     */
    private static Resource<Bookmark> toResource(Bookmark bookmark, String userId) {
        return new Resource(bookmark,

                // Create a raw link using a URI and a rel
                new Link(bookmark.getUri(), "bookmark-uri"),

                // Create a link to a the collection of bookmarks associated with the user
                linkTo(methodOn(BookmarkRestController.class).readBookmarks(userId)).withRel("bookmarks"),

                // Create a "self" link to a single bookmark
                linkTo(methodOn(BookmarkRestController.class).readBookmark(userId, bookmark.getId())).withSelfRel());
    }
}


@ControllerAdvice
class BookmarkControllerAdvice {

    @ResponseBody
    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    VndErrors userNotFoundExceptionHandler(UserNotFoundException ex) {
        return new VndErrors("error", ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(BookmarkNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    VndErrors bookmarkNotFoundExceptionHandler(BookmarkNotFoundException ex) {
        return new VndErrors("error", ex.getMessage());
    }
}

class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String userId) {
        super("could not find user '" + userId + "'.");
    }
}

class BookmarkNotFoundException extends RuntimeException {

    public BookmarkNotFoundException(Long bookMarkId) {
        super("could not find bookmark '" + bookMarkId.longValue() + "'.");
    }
}
